import numpy as np
from influxdb import InfluxDBClient


# Influx data
ip = 'localhost'
porta = 8086
login_name = 'workshop'
password = 'workshop'
database = 'timeseries'

Influx_client = InfluxDBClient(ip, porta, login_name, password, database)

def getRawSeries(hour, minute, limit):
	query = "select hour, minute, thick from some_event WHERE hour ='{}' and minute='{}' ORDER BY time DESC ".format(str(hour), str(minute))        
	query_result = Influx_client.query(query)

	result = list(query_result.get_points(measurement='some_event'))

	return result

def zscore(sample_list, value):

	mean = np.mean(sample_list)
	std = np.std(sample_list)

	if std == 0:
		z_score = 0
	else:
		z_score = (value - mean) / std

	return z_score

def alarmability(sample_list, value):
    
	z_score = zscore(sample_list, value)
	
	if (z_score > 1.96) or (z_score < -1.8):

		return value


result = getRawSeries(14, 16, 10)

thick_list = []
time_list = []

for i in result:

	time_list.append(i['time'])
	thick_list.append(i['thick'])


alarms = []

for num in thick_list:
	alarms.append(alarmability(thick_list, num))

print(alarms)




