Aula Docker: https://www.youtube.com/watch?v=FTxBa7i8VMM


### Instalar Docker:
```curl -fsSl https://get.docker.com/ | sh```

### Habilitar para o usuário:
```sudo usermod -aG docker eduardo```


## InfluxDB  
### User: workshop  
### Password: workshop 
```
$ docker run --name influxdb -d -p 8083:8083 -p 8086:8086 -e ADMIN_USER="workshop" -e INFLUXDB_INIT_PWD="workshop" -e PRE_CREATE_DB="timeseries" -e INFLUXDB_ADMIN_ENABLED=true tutum/influxdb:latest
```

## RabbitMQ  
### User: admin  
### Password: password  
```
$ sudo docker run -d --hostname pipeline --name rabbitmq -p 15672:15672 -p 5672:5672 -e RABBITMQ_DEFAULT_USER=admin -e RABBITMQ_DEFAULT_PASS=password rabbitmq:3-management
```

## Grafana  
### User: admin  
### Password: admin  
```
$ sudo docker run -d --name=grafana -p 3000:3000 grafana/grafana
```



### InfluxDb Dashboard Query
```SELECT time, thick FROM some_event```