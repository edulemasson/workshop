#!/usr/bin/env python3
import numpy as np
import pika
from time import sleep
import random


def rabbitMQ_connection(host, port, virtual_host, user, password):
	credentials = pika.PlainCredentials(user, password)
	return pika.BlockingConnection(
		pika.ConnectionParameters(host=host, port=port, virtual_host=virtual_host, credentials=credentials))


def rabbitMQ_createChannel(connection, queueName, durable=True):
	channel = connection.channel()
	channel.queue_declare(queue=queueName, durable=durable)
	return channel


def rabbitMQ_publish(body, queue, channel, exchange=''):
	channel.basic_publish(
		exchange='',
		routing_key=queue,
		body=body
	)


rabbitMQ_host = 'localhost'
rabbitMQ_port = 5672
rabbitMQ_virtualhost = '/'
rabbitMQ_user = 'admin'
rabbitMQ_password = 'password'
rabbitMQ_queue = 'outliers'
rabbitMQ_isDurable = True


rabbitMQ_connection = rabbitMQ_connection(
	rabbitMQ_host,
	rabbitMQ_port,
	rabbitMQ_virtualhost,
	rabbitMQ_user,
	rabbitMQ_password
)


rabbitMQ_channel = rabbitMQ_createChannel(rabbitMQ_connection, rabbitMQ_queue, rabbitMQ_isDurable)


mu, sigma = 500, 200 # mean and standard deviation
gaussianSample = np.random.normal(mu, sigma, 1000)

for tick in gaussianSample:
	rabbitMQ_publish(str(tick), rabbitMQ_queue, rabbitMQ_channel)
	sleep(random.choice(list(range(0,50))))
	#print(" [{}] Sent!".format(point))


print("\n\n END OF STREAMING")